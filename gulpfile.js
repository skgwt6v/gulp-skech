const gulp = require("gulp");
const del = require("del");
const sketch = require("gulp-sketch");
const imagemin = require("gulp-imagemin");
const filelog  = require("gulp-filelog");

const paths = {
  srcDir : "./sketch/**/",
  dstDir : "./assets/",
  imgDir : "assets/_src"
}

// gulp.task("sketch", () => {
//   return gulp.src("./sketch/**/*.sketch")
//     .pipe(sketch({
//       export: "slices"
//     }))
//     .pipe(gulp.dest("./assets/images/"))
// });


// gulp.task("sketch:copy", ["sketch:slices"], () => {
//   const srcGlob     = paths.dstDir;
//   const dstGlob     = paths.imgDir;
//   const splitter    = "_";
//   const pagePrefixs = {
//     "01_mobile"   : ["01_mobile"]
//   };

//   for(var page in pagePrefixs) {
//     pagePrefixs[page].forEach( ( artboard ) => {
//       gulp.src( srcGlob + "/" + page + splitter + artboard + splitter + "*" )
//         .pipe(gulp.dest( dstGlob + "/" + page + "/" + artboard ))
//         .pipe(filelog());
//     });
//   }
// });

gulp.task("watch",() => {
    gulp.watch("./sketch/**/*.sketch", ["sketch:slices"]);
});

gulp.task( "sketch:slices", () => {
  const srcGlob    = [paths.srcDir + "*.sketch" , "!" + paths.srcDir + "_*.sketch"];
  const dstGlob    = paths.dstDir;
  const sketchOptions = {
    export     : "slices"
  };

  const imageminOptions = {
    optimizationLevel: 7
  };
  return gulp.src( srcGlob )
    .pipe(sketch( sketchOptions ))
    .pipe(imagemin( imageminOptions ))
    .pipe(gulp.dest( dstGlob ))
    .pipe(filelog());
});

gulp.task('clean', function () {
  del([
    paths.dstDir + '**/**.*',
    // このファイルは削除したくないため、パターンを打ち消し
    // '!dist/mobile/deploy.json'
  ]);
});

gulp.task("default", ["clean","sketch:slices"]);
