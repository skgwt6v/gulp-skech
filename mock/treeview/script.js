  document.addEventListener("DOMContentLoaded", function(event) {
    var vm = new Vue({
      el: '#tasks-index',
      data: {
        tasks: [
        { id: 1, category: '入荷', name: '荷受（車降）', isDeleted: false, edit: false},
        { id: 2, category: '入荷', name: 'デバン', isDeleted: false, edit: false},
        { id: 3, category: '入荷', name: '検品', isDeleted: false, edit: false},
        { id: 4, category: '入荷', name: '仕分', isDeleted: false, edit: false},
        { id: 5, category: '入荷', name: '格納', isDeleted: false, edit: false},
        { id: 6, category: '入荷', name: '棚入', isDeleted: false, edit: false}
        ],
        newTask: '',
      },
      methods: {
        createTask: function () {
          if ( !this.newTask == "" ) {
            var new_id = this.tasks[this.tasks.length - 1].id + 1;
            this.tasks.push({ id: new_id, category: '入荷', name: this.newTask, isDeleted: false , edit: false});
            this.newTask = '';
          }
        },
        doneTask: function (task_id) {
          this.tasks.forEach(function (task) {
            if (task.id === task_id) {
              return task.isDeleted = true;
            }
          })
        },
        editTask: function (task_id) {
          this.tasks.forEach(function (task) {
            if (task.id === task_id) {
              return task.edit = true;
            }
          })
        },
        deleteTask: function (task_id) {
          var res = confirm("削除しよろしいですか？");
          if( res == true ) {
            return this.doneTask(task_id);
          }
          else {
            return false;
          }
        }
      },
      directives: {
        focus: {
          // ディレクティブ定義
          inserted: function (el) {
            el.focus()
          }
        }
      }
    })
    window.vm = vm;
  });